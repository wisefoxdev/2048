use bevy::{
    math::{IVec2, Vec2, Vec3},
    prelude::{
        AssetServer, BuildChildren, Color, Commands, Component, EventWriter,
        OrthographicCameraBundle, Res, ResMut, Transform,
    },
    sprite::{Sprite, SpriteBundle},
    text::{
        HorizontalAlign, Text, Text2dBundle, TextAlignment, TextSection, TextStyle, VerticalAlign,
    },
    window::{WindowDescriptor, Windows},
};

use crate::{
    game::{AddNewTile, GameFull, Score},
    tile::Tile,
    GRID_LENGTH,
};

#[derive(Component)]
pub struct ScoreBoard;

#[derive(Component)]
pub struct GameOver(pub bool);

pub fn get_window_descriptor() -> WindowDescriptor {
    WindowDescriptor {
        title: "2 0 4 8".to_string(),
        width: 640.,
        height: 740.,
        vsync: true,
        resizable: false,
        ..Default::default()
    }
}

pub fn setup(
    mut commands: Commands,
    mut events: EventWriter<AddNewTile>,
    asset_server: Res<AssetServer>,
    mut windows: ResMut<Windows>,
) {
    let window = windows.get_primary_mut().unwrap();

    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    window.set_position(IVec2::new(5, 5));

    let (w, h) = (window.width(), window.height());
    let (m_left, m_right, m_top, m_bottom) = (15., 15., 100., 15.);
    let margin = 10.;
    let tile_length = (w - m_left - m_right - GRID_LENGTH as f32 * margin) / GRID_LENGTH as f32;

    commands.insert_resource(Score(0));
    commands.insert_resource(GameOver(false));
    commands.insert_resource(GameFull(false));

    commands
        .spawn_bundle(Text2dBundle {
            transform: Transform::from_translation(Vec3::new(
                -w / 2. + 100.,
                h / 2. - m_top / 2. - m_bottom,
                0.,
            )),
            text: Text {
                sections: vec![
                    TextSection {
                        style: TextStyle {
                            font_size: 32.,
                            color: Color::BLACK,
                            font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        },
                        value: "CURRENT SCORE: ".to_string(),
                    },
                    TextSection {
                        style: TextStyle {
                            font_size: 32.,
                            color: Color::BLACK,
                            font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        },
                        value: "0".to_string(),
                    },
                ],
                alignment: TextAlignment {
                    horizontal: HorizontalAlign::Left,
                    vertical: VerticalAlign::Center,
                },
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(ScoreBoard);

    for x in 0..GRID_LENGTH {
        for y in 0..GRID_LENGTH {
            commands
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: Color::rgb(0.9, 0.9, 1.0),
                        custom_size: Some(Vec2::new(tile_length, tile_length)),
                        ..Default::default()
                    },
                    transform: Transform::from_translation(Vec3::new(
                        -w / 2.
                            + m_left
                            + tile_length / 2.
                            + tile_length * x as f32
                            + margin * x as f32,
                        -h / 2.
                            + m_bottom
                            + tile_length / 2.
                            + tile_length * y as f32
                            + margin * y as f32,
                        0.,
                    )),
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent.spawn_bundle(Text2dBundle {
                        text: Text::with_section(
                            "0".to_string(),
                            TextStyle {
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 25.,
                                color: Color::BLACK,
                            },
                            TextAlignment {
                                vertical: VerticalAlign::Center,
                                horizontal: HorizontalAlign::Center,
                            },
                        ),
                        transform: Transform::from_translation(Vec3::new(0., 0., 1.)),
                        ..Default::default()
                    });
                })
                .insert(Tile::new(x, y, 0));
        }
    }

    events.send(AddNewTile);
    events.send(AddNewTile);
}
