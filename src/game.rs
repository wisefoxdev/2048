use bevy::prelude::*;
use rand::prelude::IteratorRandom;

use crate::{
    setup::{GameOver, ScoreBoard},
    tile::Tile,
    GRID_LENGTH,
};

#[derive(Debug)]
enum Movement {
    Up,
    Down,
    Right,
    Left,
}

pub struct AddNewTile;

pub struct GamePlugin;

#[derive(Component)]
pub struct Score(pub u32);

#[derive(Component)]
pub struct GameFull(pub bool);

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<AddNewTile>()
            .add_system(spawn_new_event_listener)
            .add_system(update_score)
            .add_system(update_score_tiles)
            .add_system(movement)
            .add_system(is_game_over);
    }
}

fn spawn_new_event_listener(
    mut query: Query<&mut Tile>,
    mut events: EventReader<AddNewTile>,
    mut game_full: ResMut<GameFull>,
) {
    for _ in events.iter() {
        // Event itself is unused, this should always be called after all values are moved into new tiles
        let tile = query
            .iter_mut()
            .filter(|t| t.value == 0)
            .choose(&mut rand::thread_rng());
        match tile {
            None => game_full.0 = true,
            Some(mut tile) => tile.init(),
        }
    }
}

fn update_score_tiles(
    mut query: Query<(&mut Sprite, &Children, &Tile), Changed<Tile>>,
    mut text_query: Query<&mut Text>,
) {
    for (mut sprite, children, tile) in query.iter_mut() {
        let score_closeness = if tile.value > 0 {
            (tile.value as f32).log2()
        } else {
            0.
        } / 11.;
        let base_color = Color::ANTIQUE_WHITE;
        let target_color = Color::ORANGE_RED;
        sprite.color =
            Color::from(Vec4::from(base_color).lerp(Vec4::from(target_color), score_closeness));
        for child in children.iter() {
            if let Ok(mut text) = text_query.get_mut(*child) {
                text.sections[0].value = tile.value.to_string();
            }
        }
    }
}

fn update_score(mut query: Query<&mut Text, With<ScoreBoard>>, score: Res<Score>) {
    for mut text in query.iter_mut() {
        text.sections[1].value = score.0.to_string();
    }
}

fn movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<&mut Tile>,
    mut score: ResMut<Score>,
    mut events: EventWriter<AddNewTile>,
) {
    if let Some(movement) = match keyboard_input.get_just_pressed().last() {
        Some(KeyCode::A) => Some(Movement::Left),
        Some(KeyCode::W) => Some(Movement::Up),
        Some(KeyCode::D) => Some(Movement::Right),
        Some(KeyCode::S) => Some(Movement::Down),
        _ => None,
    } {
        let horizontal = matches!(movement, Movement::Left) || matches!(movement, Movement::Right);
        for i in 0..GRID_LENGTH {
            // check for all 0 and skip this
            if query.iter().all(|t| t.value == 0) {
                continue;
            }

            let mut tiles = query
                .iter_mut()
                .filter(|t| if horizontal { t.y == i } else { t.x == i })
                .collect::<Vec<Mut<Tile>>>();

            match movement {
                Movement::Left => tiles.sort_by_key(|t| t.x),
                Movement::Right => {
                    tiles.sort_by_key(|t| t.x);
                    tiles.reverse();
                }
                Movement::Down => tiles.sort_by_key(|t| t.y),
                Movement::Up => {
                    tiles.sort_by_key(|t| t.y);
                    tiles.reverse();
                }
            }
            //println!("i is: {}, tile_array is: {:?}", i, tiles);
            let mut tile_values = tiles.iter().map(|t| t.value).collect::<Vec<u32>>();

            for j in 0..(GRID_LENGTH - 1) {
                // if everything between now and end is 0, just be done with this loop.
                if tile_values[j..GRID_LENGTH].into_iter().all(|&v| v == 0) {
                    break;
                }
                // if value 0 move this to the back.
                while tile_values.get(j).unwrap() == &0 {
                    tile_values.remove(j);
                    tile_values.push(0);
                }
                // check if next value can go in, if so, fold into and set that value to 0.
                for k in (j + 1)..GRID_LENGTH {
                    if tile_values[k] == 0 {
                        continue;
                    }
                    if tile_values[j] == tile_values[k] {
                        tile_values[j] = tile_values[j] + tile_values[k];
                        score.0 += tile_values[j];
                        tile_values[k] = 0;
                    } else {
                        // values don't match, so no folding into
                        break;
                    }
                }
            }
            // println!("i is: {}, tile_values are: {:?}", i, tile_values);
            for j in 0..GRID_LENGTH {
                let tile = tiles.get_mut(j).unwrap();
                tile.value = tile_values[j];
            }
        }
        // spawn new tile!
        events.send(AddNewTile);
    }
}

fn is_game_over(
    query: Query<&Tile>,
    mut game_over: ResMut<GameOver>,
    game_full: Res<GameFull>,
    mut text_query: Query<&mut Text, With<ScoreBoard>>,
) {
    if query.iter().any(|t| t.value >= 2048) {
        for mut text in text_query.iter_mut() {
            text.sections[0].value = "YOU WIN! ".to_string();
            game_over.0 = true;
        }
    }
    if game_full.0 && !game_over.0 {
        for i in 0..GRID_LENGTH {
            let mut tiles_horizontal = query.iter().filter(|t| t.y == i).collect::<Vec<&Tile>>();
            tiles_horizontal.sort_by_key(|&t| t.x);
            let mut tiles_vertical = query.iter().filter(|t| t.x == i).collect::<Vec<&Tile>>();
            tiles_vertical.sort_by_key(|&t| t.y);

            for j in 0..(GRID_LENGTH - 1) {
                if tiles_horizontal[j].value == tiles_horizontal[j + 1].value {
                    return;
                }
                if tiles_vertical[j].value == tiles_vertical[j + 1].value {
                    return;
                }
            }
        }
        // We're still here, so the game is over.
        for mut text in text_query.iter_mut() {
            text.sections[0].value = "GAME OVER! ".to_string();
            game_over.0 = true;
        }
    }
}
