mod game;
mod setup;
mod tile;

use bevy::prelude::*;
use game::GamePlugin;
use setup::{get_window_descriptor, setup};

const GRID_LENGTH: usize = 4;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.9, 0.9, 0.9)))
        .insert_resource(get_window_descriptor())
        .add_plugins(DefaultPlugins)
        .add_plugin(GamePlugin)
        .add_startup_system(setup)
        .run();
}
