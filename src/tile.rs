use bevy::prelude::Component;

#[derive(Component, Debug)]
pub struct Tile {
    pub value: u32,
    pub x: usize,
    pub y: usize,
}

impl Tile {
    pub fn new(x: usize, y: usize, value: u32) -> Tile {
        Tile { x, y, value }
    }

    pub fn init(&mut self) {
        self.value = 2;
    }
}
